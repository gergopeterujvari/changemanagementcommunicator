﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Resources;
using System.Reflection;
using System.Security;
using Microsoft.SharePoint.Client;
using System.IO;

namespace OIMChangeManagerCommunicator
{
    
    public class ResourceForEnviorment
    {
        public string host;
        public string env;
        public string fileNameFormat;
        public string savePath;
        public string username;
        public string password;
        public ResourceForEnviorment(string host, string env, string filenameFormat, string savepath, string username, string password)
        {
            this.host = host;
            this.env = env;
            this.fileNameFormat = filenameFormat;
            this.savePath = savepath;
            this.username = username;
            this.password = password;
        }
    }
    class Program
    {
        
        static async Task Main(string[] args)
        {
            var lstENV = new List<ResourceForEnviorment>();
            lstENV.Add(new ResourceForEnviorment(DEV.host, DEV.env, DEV.fileNameFormat, DEV.savePath, DEV.username, DEV.password));
            lstENV.Add(new ResourceForEnviorment(PROD.host, PROD.env, PROD.fileNameFormat, PROD.savePath, PROD.username, PROD.password));
            lstENV.Add(new ResourceForEnviorment(UAT.host, UAT.env, UAT.fileNameFormat, UAT.savePath, UAT.username, UAT.password));
            
            foreach (var item in lstENV)
            {
                string host = item.host;
                string authUrl = $@"{host}/auth/apphost";
                string logoutUrl = $@"{host}/auth/logout";
                string apiUrl = $@"{host}/api";

                var filename = item.fileNameFormat.Replace("<ENV>", item.env);
                var filepath = String.Format(@"{0}{1}", item.savePath, filename);
                var httpClientHandler = new HttpClientHandler();
                // Return `true` to allow certificates that are untrusted/invalid
                httpClientHandler.ServerCertificateCustomValidationCallback =
                    HttpClientHandler.DangerousAcceptAnyServerCertificateValidator;
                var httpClient = new HttpClient(httpClientHandler);
                httpClient.DefaultRequestHeaders
                      .Accept
                      .Add(new MediaTypeWithQualityHeaderValue("application/json"));

                try
                {
                    string authBody = "{\"AuthString\":\"Module=DialogUser;User=" + item.username + ";Password=" + item.password + "\"}";
                    var authResponse = await httpClient.PostAsync($"{authUrl}", new StringContent(authBody, Encoding.UTF8, "application/json"));
                    authResponse.EnsureSuccessStatusCode();

                    var apiResponse = await httpClient.PutAsync($@"{apiUrl}/script/CCC_GetChangeManagementData", new StringContent("", Encoding.UTF8, "application/json"));
                    var response = await apiResponse.Content.ReadAsStringAsync();
                    StringBuilder sb = new StringBuilder(response);

                    using (System.IO.StreamWriter file = new System.IO.StreamWriter(filepath))
                    {
                        file.WriteLine(sb.ToString());
                    }

                    apiResponse.EnsureSuccessStatusCode();

                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    string logoutBody = "{}";
                    var logoutResponse = await httpClient.PostAsync($"{logoutUrl}", new StringContent(logoutBody, Encoding.UTF8, "application/json"));
                    logoutResponse.EnsureSuccessStatusCode();
                }
            }
            
        }
    }
}

